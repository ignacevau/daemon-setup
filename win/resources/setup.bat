copy "installer\win\resources\nssm.exe" "smart230\resources\win\"
@REM create virtual env for "download.py"
python -m virtualenv installer\download\virt --python=python3.8
@REM install modules for the virtual env
installer\download\virt\Scripts\pip.exe install -r installer\download\requirements.txt
@REM use virtual env to run download script
installer\download\virt\Scripts\python.exe installer\download\download.py
@REM create virtual env for the daemon helper service
python -m virtualenv smart230\daemon_helper\source\virt --python=python3.8
@REM install modules for the virtual env
smart230\daemon_helper\source\virt\Scripts\pip.exe install -r smart230\daemon_helper\source\requirements.txt
@REM use nssm to create the service that runs daemon helper files using python in the virtual env
smart230\resources\win\nssm.exe remove Smart230Service confirm
smart230\resources\win\nssm.exe install Smart230Service %1 %2
smart230\resources\win\nssm.exe set Smart230Service AppDirectory %3
smart230\resources\win\nssm.exe start Smart230Service