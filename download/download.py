import requests
from git.repo.base import Repo
import shutil
import os
import stat


SERVER_PING_URL = "https://d12zqkyuq0ro7o.cloudfront.net/ping"
SERVER_DAEMON_HELPER_GITURL = "https://d12zqkyuq0ro7o.cloudfront.net/daemon-helper-url"
SERVER_DAEMON_CLIENT_GITURL = "https://d12zqkyuq0ro7o.cloudfront.net/daemon-client-url"


def remove_readonly(fn, path):
    try:
        os.chmod(path, stat.S_IWRITE)
        fn(path)
    except Exception as exc:
        print("Skipped path: %s. Reason: %s" % (path, exc))


print("Connecting to server...")

try:
    ping = requests.get(SERVER_PING_URL)
    if (ping.text != "pong"):
        raise
except:
    raise Exception("no internet connection")

print("Connection to server established.")

daemon_helper_url = ""
daemon_client_url = ""
try:
    daemon_helper_url = requests.get(SERVER_DAEMON_HELPER_GITURL).text
    daemon_client_url = requests.get(SERVER_DAEMON_CLIENT_GITURL).text
except:
    raise Exception("Couldn't get source location of daemon files from server")


try:
    print("\nDownloading daemon helper files...")
    repo = Repo.clone_from(daemon_helper_url, "smart230/daemon_helper")
    print("Downloaded daemon helper files")
    print("\nDownloading daemon client files...")
    repo = Repo.clone_from(daemon_client_url, "smart230/daemon_client")
    print("Downloaded daemon client files")
except:
    raise Exception("Couldn't download required files")